using DifferentialEquations, Interpolations, OrdinaryDiffEq
using MAT, LinearAlgebra, Plots, PreallocationTools, Statistics, BenchmarkTools, StaticArrays, NaNMath, PyCall, ScikitLearn
@pyimport sklearn.metrics as metrics

#
# loading PCA files
#

n_file = 25

pca_f_l = open("pca_f_l_$(n_file).pkl", "r") do io
    pca_model_data = read(io)  
    pyimport("pickle").loads(pca_model_data)
end
pca_s_l = open("pca_s_l_$(n_file).pkl", "r") do io
    pca_model_data = read(io)  
    pyimport("pickle").loads(pca_model_data)
end

pca_f_nl = open("pca_f_nl_$(n_file).pkl", "r") do io
    pca_model_data = read(io)  
    pyimport("pickle").loads(pca_model_data)
end

pca_s_nl = open("pca_s_nl_$(n_file).pkl", "r") do io
    pca_model_data = read(io)  
    pyimport("pickle").loads(pca_model_data)
end

#
# Loading NN
#

import ONNXRunTime as ORT

model = ORT.load_inference("model_$(n_file).onnx")

#
#
#

function T_seq_gen(t, minValue, maxValue)
    minDuration = 0.3
    maxDuration = 0.6
    maxConstantDuration = 0.15

    totalTime = t[end]
    remainingTime = totalTime
    currentMinValue = minValue
    y = zeros(length(t))

    currentIndex = 1
    lastPhaseType = -1

    while remainingTime > 0 && currentIndex <= length(t)
        phaseDuration = minDuration + (maxDuration - minDuration) * rand()
        phaseDuration = min(phaseDuration, remainingTime)

        endTime = t[currentIndex] + phaseDuration
        if endTime > totalTime
            endTime = totalTime
        end

        endIndex = findfirst(x -> x >= endTime, t)
        if isnothing(endIndex)
            endIndex = length(t)
        end

        numSamples = endIndex - currentIndex + 1

        if numSamples <= 1
            numSamples = 2
        end

        # Ensure that the constant phase does not repeat consecutively
        if lastPhaseType == 2
            phaseTypeOptions = [1, 3]
        else
            phaseTypeOptions = [1, 2, 3]
        end
        phaseType = phaseTypeOptions[rand(1:length(phaseTypeOptions))]

        x = range(-6, stop=6, length=numSamples)
        if phaseType == 1
            finalValue = currentMinValue + (maxValue - currentMinValue) * rand()
            phaseValues = currentMinValue .+ (finalValue - currentMinValue) .* (1.0 ./ (1.0 .+ exp.(-x)))
            currentMinValue = finalValue
        elseif phaseType == 2
            phaseDuration = min(phaseDuration, maxConstantDuration)
            phaseValues = fill(currentMinValue, numSamples)
        elseif phaseType == 3
            finalValue = minValue + (currentMinValue - minValue) * rand()
            phaseValues = finalValue .+ (currentMinValue - finalValue) .* (1.0 ./ (1.0 .+ exp.(x)))
            currentMinValue = finalValue
        end

        if currentIndex + length(phaseValues) - 1 <= length(y)
            y[currentIndex:currentIndex+length(phaseValues)-1] = phaseValues
        else
            y[currentIndex:end] = phaseValues[1:(length(y)-currentIndex+1)]
        end

        currentIndex += length(phaseValues)
        if currentIndex > length(t)
            break
        end
        remainingTime = totalTime - t[currentIndex - 1]
        lastPhaseType = phaseType
    end

    return y
end

function m_seq_gen(timeVec, minValue, maxValue)
    output = zeros(length(timeVec))
    
    currentTime = timeVec[1]
    currentValue = (maxValue - minValue) * rand() + minValue
    
    i = 1
    while currentTime < timeVec[end]
        stepDuration = rand() * (1.0 - 0.8) + 0.8
        
        nextTime = currentTime + stepDuration
        
        while i <= length(timeVec) && timeVec[i] <= nextTime
            output[i] = currentValue
            i += 1
        end
        
        currentTime = nextTime
        currentValue = (maxValue - minValue) * rand() + minValue
    end
    
    return output
end

function collect_elements(vectors)
    return [collect(v) for v in vectors]
end

function find_nearest_indices(x_ref::AbstractArray{Float64}, x_act::AbstractArray{Float64})::Vector{Int}
    nearest_indices = Vector{Int}(undef, length(x_act))

    for (i, x) in enumerate(x_act)
        nearest_index = argmin(abs.(x_ref .- x))
        nearest_indices[i] = nearest_index
    end

    return nearest_indices
end

function mse(A,B)
    return (metrics.mean_squared_error(A, B))
end

function error_percentage(A,B)
    return (metrics.mean_absolute_error(A, B) / mean(A))*100
end

function r2(A,B)
    return metrics.r2_score(A, B)
end

function calculate_error(metric, A, B)
    errors = [metric(A[i, :], B[i, :]) for i in eachindex(axes(A, 1))]
    return mean(errors)
end

#
#
#

function generate_interpolations(N, time_total_c, time_total_dc)

    T_c_seqs = []
    T_dc_seqs = []
    m_c_seqs = []
    m_dc_seqs = []
    t_cs = []
    t_dcs = []
    
    for _ in 1:N
        t_c = range(0.0, stop=time_total_c, length=Int(time_total_c / 0.05 + 1))
        t_dc = range(0.0, stop=time_total_dc, length=Int(time_total_dc / 0.05 + 1))

        T_seq_c = vec(T_seq_gen(t_c, 0.6, 1.0))
        T_seq_dc = vec(T_seq_gen(t_dc, 0.0, 0.2))

        m_seq_c = vec(m_seq_gen(t_c, 0.45, 0.65))
        m_seq_dc = vec(m_seq_gen(t_dc, 0.45, 0.65))

        T_seq_c = @. max(Float64(real(T_seq_c)))
        T_seq_dc = @. max(Float64(real(T_seq_dc)))
        m_seq_c = @. max(Float64(real(m_seq_c)))
        m_seq_dc = @. max(Float64(real(m_seq_dc)))

        push!(t_cs, t_c)
        push!(t_dcs, t_dc)
        push!(T_c_seqs, T_seq_c)
        push!(T_dc_seqs, T_seq_dc)
        push!(m_c_seqs, m_seq_c)
        push!(m_dc_seqs, m_seq_dc)
    end

    return t_cs, t_dcs, T_c_seqs, T_dc_seqs, m_c_seqs, m_dc_seqs
end

function generate_interpolations_test(N, time_total_c, time_total_dc)

    T_c_seqs = []
    T_dc_seqs = []
    m_c_seqs = []
    m_dc_seqs = []
    t_cs = []
    t_dcs = []
    
    for _ in 1:N
        t_c = range(0.0, stop=time_total_c, length=Int(time_total_c / 0.1 + 1))
        t_dc = range(0.0, stop=time_total_dc, length=Int(time_total_dc / 0.1 + 1))

        T_seq_c = vec(T_seq_gen(t_c, 0.6, 1.0))
        T_seq_dc = vec(T_seq_gen(t_dc, 0.0, 0.2))

        m_seq_c = vec(m_seq_gen(t_c, 0.45, 0.65))
        m_seq_dc = vec(m_seq_gen(t_dc, 0.45, 0.65))

        T_seq_c = @. max(Float64(real(T_seq_c)))
        T_seq_dc = @. max(Float64(real(T_seq_dc)))
        m_seq_c = @. max(Float64(real(m_seq_c)))
        m_seq_dc = @. max(Float64(real(m_seq_dc)))

        push!(t_cs, t_c)
        push!(t_dcs, t_dc)
        push!(T_c_seqs, T_seq_c)
        push!(T_dc_seqs, T_seq_dc)
        push!(m_c_seqs, m_seq_c)
        push!(m_dc_seqs, m_seq_dc)
    end

    return t_cs, t_dcs, T_c_seqs, T_dc_seqs, m_c_seqs, m_dc_seqs
end

n_cycles = 4 
time_total_c = 10.0
time_total_dc = 10.0

t_c_ = range(0.0, stop=time_total_c, length=Int(time_total_c / 0.05 + 1))
t_dc_ = range(0.0, stop=time_total_dc, length=Int(time_total_dc / 0.05 + 1))

t_cs, t_dcs, T_c_seqs, T_dc_seqs, m_c_seqs, m_dc_seqs = generate_interpolations(n_cycles, time_total_c, time_total_dc)

t_cs_test, t_dcs_test, T_c_seqs_test, T_dc_seqs_test, m_c_seqs_test, m_dc_seqs_test = generate_interpolations_test(n_cycles, time_total_c, time_total_dc)

#
# Girds
#

n_l = 1000
x_l = range(0.0, stop=1.0, length=n_l)
dx_l = 1/n_l

n_h = n_file
x_h = range(0.0, stop=1.0, length=n_h)
dx_h = 1/n_h

n_nl = 1000
x_nl = range(0.0, stop=1.0, length=n_nl)
dx_nl = 1/n_nl

indices = find_nearest_indices(x_nl, x_h)

#
# Linear model
#

const D_t = 1.0
const D_r = 0.03
const epsilon = 0.27
const rho_f_l = 845
const cp_f_l = 2380
const k_f_l = 0.1
const mu_f_l = 5.7e-4
const rho_s_l = 2595
const cp_s_l = 900
const k_s_l = 5.5
const L = 3.0
const st = 1500
const rho_steel_l = 7900
const cp_steel_l = 490
const mass_cylinder = 0.3713 * rho_steel_l
const rho_s_crr_l = rho_s_l + (mass_cylinder * cp_steel_l / ((1 - epsilon) * cp_s_l * 0.3713))
const nita_f_l = (k_f_l * st) / (epsilon * rho_f_l * cp_f_l * (L ^ 2))
const theta_f_l = ((1.55 / D_r) * st) / (epsilon * rho_f_l * cp_f_l)
const nita_s_l = (k_s_l * st) / ((1 - epsilon) * rho_s_crr_l * cp_s_l * (L ^ 2))

function Linear_model!(dTdt, T, p, t)

    (dx, n, epsilon, L, D_t, st, rho_f, cp_f, k_f, mu_f, cp_s, rho_s_crr, nita_f, theta_f, nita_s, m_in_func, T_in_func) = p

    m_dot =  m_in_func(t)
    T_f_in = T_in_func(t)

    T_f = @view T[1:n]
    T_s = @view T[n+1:2*n]
    
    h_v = (6 * (1 - epsilon) * k_f * (2 + 1.1 * (((m_dot * D_r * 4) / (mu_f * π * D_t * D_t)) ^ 0.6) * (((mu_f * cp_f) / k_f) ^ 0.3333))) / (D_r * D_r)
    Pe = (((m_dot * 4) / (rho_f * π * D_t * D_t * epsilon))*st/L)/(2*dx)
    nita_s_ = nita_s/(dx^2)
    nita_f_ = nita_f/(dx^2)
    eita_f = ((h_v * st) / (epsilon * rho_f * cp_f))
    eita_s = ((h_v * st) / ((1 - epsilon) * rho_s_crr * cp_s))

    @inbounds dTdt[1] = -Pe * (T_f[2] - T_f_in) + nita_f_ * (T_f[2] - 2*T_f_in + T_f_in) +  eita_f * (T_s[1] - T_f[1]) - theta_f * (T_f[1] + 0.566)
    @inbounds dTdt[n] = -Pe * (T_f[n] - T_f[n-1]) + nita_f_ * (T_f[n] - 2*T_f[n] + T_f[n-1]) +  eita_f * (T_s[n] - T_f[n]) - theta_f * (T_f[n] + 0.566)
    @inbounds dTdt[n+1] = nita_s_ * (T_s[2] - 2*T_s[1] + T_s[1]) - eita_s * (T_s[1] - T_f[1])
    @inbounds dTdt[2*n] = nita_s_ * (T_s[n] - 2*T_s[n] + T_s[n-1]) - eita_s * (T_s[n] - T_f[n])

    @inbounds for i = 2:n-1
        @views dTdt[i] = -Pe * (T_f[i+1] - T_f[i-1]) + nita_f_ * (T_f[i+1] - 2*T_f[i] + T_f[i-1]) +  eita_f * (T_s[i] - T_f[i]) - theta_f * (T_f[i] + 0.566)
        @views dTdt[n+i] = nita_s_ * (T_s[i+1] - 2*T_s[i] + T_s[i-1]) - eita_s * (T_s[i] - T_f[i])
    end
    
    nothing
end

f_L_c_full = Vector{Any}()
s_L_c_full = Vector{Any}()
f_L_dc_full = Vector{Any}()
s_L_dc_full = Vector{Any}()

T0_l = [zeros(n_l); zeros(n_l)]

function LINEAR_MODEL(T0_l, n_l, n_cycles, t_cs, T_c_seqs, t_dcs, T_dc_seqs, m_c_seqs, m_dc_seqs, dx_l, epsilon, L, D_t, st, rho_f_l, cp_f_l, k_f_l, mu_f_l, cp_s_l, rho_s_crr_l, nita_f_l, theta_f_l, nita_s_l, time_total_c, time_total_dc)
    for i=1:n_cycles

        T_c_func = Interpolations.LinearInterpolation(t_cs[i], T_c_seqs[i])
        T_dc_func = Interpolations.LinearInterpolation(t_dcs[i], T_dc_seqs[i])
        m_c_func = Interpolations.LinearInterpolation(t_cs[i], m_c_seqs[i])
        m_dc_func = Interpolations.LinearInterpolation(t_dcs[i], m_dc_seqs[i])
        
        #
        # Charging
        #

        tspan = (0.0, time_total_c)
        p_l_1 = (dx_l, n_l, epsilon, L, D_t, st, rho_f_l, cp_f_l, k_f_l, mu_f_l, cp_s_l, rho_s_crr_l, nita_f_l, theta_f_l, nita_s_l, m_c_func, T_c_func)
        prob_l_1 = ODEProblem(Linear_model!, T0_l, tspan, p_l_1)

        sol_l_1 = solve(prob_l_1, RDPK3Sp35(), reltol=1e-4, saveat=t_cs[i])

        t_l_1 = sol_l_1.t
        T_l_1 = Array(sol_l_1)

        f_L_c = T_l_1[1:n_l,:]
        s_L_c = T_l_1[n_l+1:2*n_l,:]

        T0_l = [reverse(f_L_c[:,end]); reverse(s_L_c[:,end])]

        #
        # Discharging
        #

        tspan = (0.0, time_total_dc)
        p_l_2 = (dx_l, n_l, epsilon, L, D_t, st, rho_f_l, cp_f_l, k_f_l, mu_f_l, cp_s_l, rho_s_crr_l, nita_f_l, theta_f_l, nita_s_l, m_dc_func, T_dc_func)
        prob_l_2 = ODEProblem(Linear_model!, T0_l, tspan, p_l_2)

        sol_l_2 = solve(prob_l_2, RDPK3Sp35(), reltol=1e-4, saveat=t_dcs[i])

        t_l_2 = sol_l_2.t
        T_l_2 = Array(sol_l_2)

        f_L_dc = T_l_2[1:n_l,:]
        s_L_dc = T_l_2[n_l+1:2*n_l,:]

        T0_l = [reverse(f_L_dc[:,end]); reverse(s_L_dc[:,end])]

        #
        #
        #

        push!(f_L_c_full, collect(f_L_c .* 150 .+ 100))
        push!(s_L_c_full, collect(s_L_c .* 150 .+ 100))

        push!(f_L_dc_full, collect(f_L_dc .* 150 .+ 100))
        push!(s_L_dc_full, collect(s_L_dc .* 150 .+ 100))

    end
end

function LINEAR_MODEL_b!(T0_l, n_l, n_cycles, t_cs, T_c_seqs, t_dcs, T_dc_seqs, m_c_seqs, m_dc_seqs, dx_l, epsilon, L, D_t, st, rho_f_l, cp_f_l, k_f_l, mu_f_l, cp_s_l, rho_s_crr_l, nita_f_l, theta_f_l, nita_s_l, time_total_c, time_total_dc)
    for i=1:n_cycles

        T_c_func = Interpolations.LinearInterpolation(t_cs[i], T_c_seqs[i])
        T_dc_func = Interpolations.LinearInterpolation(t_dcs[i], T_dc_seqs[i])
        m_c_func = Interpolations.LinearInterpolation(t_cs[i], m_c_seqs[i])
        m_dc_func = Interpolations.LinearInterpolation(t_dcs[i], m_dc_seqs[i])
        
        #
        # Charging
        #

        tspan = (0.0, time_total_c)
        p_l_1 = (dx_l, n_l, epsilon, L, D_t, st, rho_f_l, cp_f_l, k_f_l, mu_f_l, cp_s_l, rho_s_crr_l, nita_f_l, theta_f_l, nita_s_l, m_c_func, T_c_func)
        prob_l_1 = ODEProblem(Linear_model!, T0_l, tspan, p_l_1)

        sol_l_1 = solve(prob_l_1, RDPK3Sp35(), reltol=1e-4, saveat=t_cs[i])

        t_l_1 = sol_l_1.t
        T_l_1 = Array(sol_l_1)

        f_L_c = T_l_1[1:n_l,:]
        s_L_c = T_l_1[n_l+1:2*n_l,:]

        T0_l = [reverse(f_L_c[:,end]); reverse(s_L_c[:,end])]

        #
        # Discharging
        #

        tspan = (0.0, time_total_dc)
        p_l_2 = (dx_l, n_l, epsilon, L, D_t, st, rho_f_l, cp_f_l, k_f_l, mu_f_l, cp_s_l, rho_s_crr_l, nita_f_l, theta_f_l, nita_s_l, m_dc_func, T_dc_func)
        prob_l_2 = ODEProblem(Linear_model!, T0_l, tspan, p_l_2)

        sol_l_2 = solve(prob_l_2, RDPK3Sp35(), reltol=1e-4, saveat=t_dcs[i])

        t_l_2 = sol_l_2.t
        T_l_2 = Array(sol_l_2)

        f_L_dc = T_l_2[1:n_l,:]
        s_L_dc = T_l_2[n_l+1:2*n_l,:]

        T0_l = [reverse(f_L_dc[:,end]); reverse(s_L_dc[:,end])]

    end
end

LINEAR_MODEL(T0_l, n_l, n_cycles, t_cs_test, T_c_seqs_test, t_dcs_test, T_dc_seqs_test, m_c_seqs_test, m_dc_seqs_test, dx_l, epsilon, L, D_t, st, rho_f_l, cp_f_l, k_f_l, mu_f_l, cp_s_l, rho_s_crr_l, nita_f_l, theta_f_l, nita_s_l, time_total_c, time_total_dc)

#
# Hybrid model
#


f_H_c_full = Vector{Any}()
s_H_c_full = Vector{Any}()

f_H_dc_full = Vector{Any}()
s_H_dc_full = Vector{Any}()

T0_h = [zeros(n_h); zeros(n_h)]

function HYBRID_MODEL(T0_h, n_h, n_cycles, t_cs, T_c_seqs, t_dcs, T_dc_seqs, m_c_seqs, m_dc_seqs, dx_h, epsilon, L, D_t, st, rho_f_l, cp_f_l, k_f_l, mu_f_l, cp_s_l, rho_s_crr_l, nita_f_l, theta_f_l, nita_s_l, time_total_c, time_total_dc)
    for i=1:n_cycles

        T_c_func = Interpolations.LinearInterpolation(t_cs[i], T_c_seqs[i])
        T_dc_func = Interpolations.LinearInterpolation(t_dcs[i], T_dc_seqs[i])
        m_c_func = Interpolations.LinearInterpolation(t_cs[i], m_c_seqs[i])
        m_dc_func = Interpolations.LinearInterpolation(t_dcs[i], m_dc_seqs[i])
        
        #
        # Charging
        #

        tspan = (0.0, time_total_c)
        p_h_1 = (dx_h, n_h, epsilon, L, D_t, st, rho_f_l, cp_f_l, k_f_l, mu_f_l, cp_s_l, rho_s_crr_l, nita_f_l, theta_f_l, nita_s_l, m_c_func, T_c_func)
        prob_h_1 = ODEProblem(Linear_model!, T0_h, tspan, p_h_1)

        sol_h_1 = solve(prob_h_1, RDPK3Sp35(), reltol=1e-4, saveat=t_cs[i])

        t_h_1 = sol_h_1.t
        T_h_1 = Array(sol_h_1)

        f_H_c = T_h_1[1:n_h,:]
        s_H_c = T_h_1[n_h+1:2*n_h,:]

        f_H_c_pca = pca_f_l.transform(f_H_c')
        s_H_c_pca = pca_s_l.transform(s_H_c')
        
        ip_1 = Float32.(hcat(reshape(t_cs[i],:,1), reshape(T_c_seqs[i],:,1), reshape(m_c_seqs[i],:,1), f_H_c_pca, s_H_c_pca))

        #input_batches = [Dict("input" => ip_1[k:k, :]) for k in 1:size(ip_1, 1)]
        #outputs = [model(batch) for batch in input_batches]
        #op_1 = reduce(vcat, [out["output"] for out in outputs])
        
        input_batch = Dict("input" => ip_1)
        output = model(input_batch)
        op_1 = output["output"]
        
        f_H_c_ = (pca_f_nl.inverse_transform(op_1[:,1:15]))'
        s_H_c_ = (pca_s_nl.inverse_transform(op_1[:,16:end]))'

        T0_h = [reverse(f_H_c_[indices,end]); reverse(s_H_c_[indices,end])]

        #
        # Discharging
        #

        tspan = (0.0, time_total_dc)
        p_h_2 = (dx_h, n_h, epsilon, L, D_t, st, rho_f_l, cp_f_l, k_f_l, mu_f_l, cp_s_l, rho_s_crr_l, nita_f_l, theta_f_l, nita_s_l, m_dc_func, T_dc_func)
        prob_h_2 = ODEProblem(Linear_model!, T0_h, tspan, p_h_2)

        sol_h_2 = solve(prob_h_2, RDPK3Sp35(), reltol=1e-4, saveat=t_dcs[i])

        t_h_2 = sol_h_2.t
        T_h_2 = Array(sol_h_2)

        f_H_dc = T_h_2[1:n_h,:]
        s_H_dc = T_h_2[n_h+1:2*n_h,:]

        f_H_dc_pca = pca_f_l.transform(f_H_dc')
        s_H_dc_pca = pca_s_l.transform(s_H_dc')
        
        ip_2 = Float32.(hcat(reshape(t_dcs[i],:,1), reshape(T_dc_seqs[i],:,1), reshape(m_dc_seqs[i],:,1), f_H_dc_pca, s_H_dc_pca))
        
        input_batch = Dict("input" => ip_2)
        output = model(input_batch)
        op_2 = output["output"]
        
        f_H_dc_ = (pca_f_nl.inverse_transform(op_2[:,1:15]))'
        s_H_dc_ = (pca_s_nl.inverse_transform(op_2[:,16:end]))'

        T0_h = [reverse(f_H_dc_[indices,end]); reverse(s_H_dc_[indices,end])]

        #
        #
        #

        push!(f_H_c_full, collect(f_H_c_ .* 150 .+ 100))
        push!(s_H_c_full, collect(s_H_c_ .* 150 .+ 100))

        push!(f_H_dc_full, collect(f_H_dc_ .* 150 .+ 100))
        push!(s_H_dc_full, collect(s_H_dc_ .* 150 .+ 100))

    end
end

@inline function HYBRID_MODEL_b!(T0_h, n_h, n_cycles, t_cs, T_c_seqs, t_dcs, T_dc_seqs, m_c_seqs, m_dc_seqs, dx_h, epsilon, L, D_t, st, rho_f_l, cp_f_l, k_f_l, mu_f_l, cp_s_l, rho_s_crr_l, nita_f_l, theta_f_l, nita_s_l, time_total_c, time_total_dc)
    for i=1:n_cycles

        T_c_func = Interpolations.LinearInterpolation(t_cs[i], T_c_seqs[i])
        T_dc_func = Interpolations.LinearInterpolation(t_dcs[i], T_dc_seqs[i])
        m_c_func = Interpolations.LinearInterpolation(t_cs[i], m_c_seqs[i])
        m_dc_func = Interpolations.LinearInterpolation(t_dcs[i], m_dc_seqs[i])
        
        #
        # Charging
        #

        tspan = (0.0, time_total_c)
        p_h_1 = (dx_h, n_h, epsilon, L, D_t, st, rho_f_l, cp_f_l, k_f_l, mu_f_l, cp_s_l, rho_s_crr_l, nita_f_l, theta_f_l, nita_s_l, m_c_func, T_c_func)
        prob_h_1 = ODEProblem(Linear_model!, T0_h, tspan, p_h_1)

        sol_h_1 = solve(prob_h_1, CKLLSRK43_2(), reltol=1e-4, saveat=t_cs[i])

        t_h_1 = sol_h_1.t
        T_h_1 = Array(sol_h_1)

        f_H_c = T_h_1[1:n_h,:]
        s_H_c = T_h_1[n_h+1:2*n_h,:]

        f_H_c_pca = pca_f_l.transform(f_H_c')
        s_H_c_pca = pca_s_l.transform(s_H_c')
        
        ip_1 = Float32.(hcat(reshape(t_cs[i],:,1), reshape(T_c_seqs[i],:,1), reshape(m_c_seqs[i],:,1), f_H_c_pca, s_H_c_pca))
        
        input_batch = Dict("input" => ip_1)
        output = model(input_batch)
        op_1 = output["output"]
        
        f_H_c_ = (pca_f_nl.inverse_transform(op_1[:,1:15]))'
        s_H_c_ = (pca_s_nl.inverse_transform(op_1[:,16:end]))'

        T0_h = [reverse(f_H_c_[indices,end]); reverse(s_H_c_[indices,end])]

        #
        # Discharging
        #

        tspan = (0.0, time_total_dc)
        p_h_2 = (dx_h, n_h, epsilon, L, D_t, st, rho_f_l, cp_f_l, k_f_l, mu_f_l, cp_s_l, rho_s_crr_l, nita_f_l, theta_f_l, nita_s_l, m_dc_func, T_dc_func)
        prob_h_2 = ODEProblem(Linear_model!, T0_h, tspan, p_h_2)

        sol_h_2 = solve(prob_h_2, CKLLSRK43_2(), reltol=1e-4, saveat=t_dcs[i])

        t_h_2 = sol_h_2.t
        T_h_2 = Array(sol_h_2)

        f_H_dc = T_h_2[1:n_h,:]
        s_H_dc = T_h_2[n_h+1:2*n_h,:]

        f_H_dc_pca = pca_f_l.transform(f_H_dc')
        s_H_dc_pca = pca_s_l.transform(s_H_dc')
        
        ip_2 = Float32.(hcat(reshape(t_dcs[i],:,1), reshape(T_dc_seqs[i],:,1), reshape(m_dc_seqs[i],:,1), f_H_dc_pca, s_H_dc_pca))
        
        input_batch = Dict("input" => ip_2)
        output = model(input_batch)
        op_2 = output["output"]
        
        f_H_dc_ = (pca_f_nl.inverse_transform(op_2[:,1:15]))'
        s_H_dc_ = (pca_s_nl.inverse_transform(op_2[:,16:end]))'

        T0_h = [reverse(f_H_dc_[indices,end]); reverse(s_H_dc_[indices,end])]
    end
end

HYBRID_MODEL(T0_h, n_h, n_cycles, t_cs_test, T_c_seqs_test, t_dcs_test, T_dc_seqs_test, m_c_seqs_test, m_dc_seqs_test, dx_h, epsilon, L, D_t, st, rho_f_l, cp_f_l, k_f_l, mu_f_l, cp_s_l, rho_s_crr_l, nita_f_l, theta_f_l, nita_s_l, time_total_c, time_total_dc)

#@benchmark HYBRID_MODEL(T0_h, n_h, n_cycles, t_cs, T_c_seqs, t_dcs, T_dc_seqs, m_c_seqs, m_dc_seqs, dx_h, epsilon, L, D_t, st, rho_f_l, cp_f_l, k_f_l, mu_f_l, cp_s_l, rho_s_crr_l, nita_f_l, theta_f_l, nita_s_l, time_total_c, time_total_dc)

#
# Non Linear model
#

function NonLinear_model!(dTdt, T, p, t)
    (dx, n, L, st, D_t, D_r, epsilon, mass_cyclinder, cp_steel, m_in_func, T_in_func) = p

    m_dot = m_in_func(t)
    T_f_in = T_in_func(t)

    T_f = @view T[1:n]
    T_s = @view T[n+1:2*n]

    T_f_ = @. T_f * 150 + 100
    T_s_ = @. T_s * 150 + 100

    T_f_1 = @. max(T_f_, 1e-6)
    T_s_1 = @. max(T_s_, 1e-6)

    rho_f = @. -0.000321 * (T_f_1^2) - 0.614254 * T_f_1 + 1020.62
    k_f = @. -1.5e-07 * (T_f_1^2) - 3.3e-05 * T_f_1 + 0.118294
    cp_f = @. 8.970785e-04 * (T_f_1^2) + 3.313 * T_f_1 + 1496.005
    mu_f = @. exp(-2.048 * log(T_f_1) + 10.773) * 1e-03

    cp_s = @. 0.8841 * T_s_1 + 795.9
    rho_s = 2595
    k_s = 5.5
    rho_s_crr = @. rho_s + (mass_cyclinder * cp_steel / ((1 - epsilon) * cp_s * 0.3713))

    Re = @. (m_dot * D_r * 4) / (mu_f * π * D_t * D_t)
    Pr = @. (mu_f * cp_f) / k_f
    h_v = @. (6 *(1-epsilon) * k_f * (2 + 1.1 * (Re^0.6) * (Pr^0.3333))) / (D_r*D_r)

    Pe = @. (((m_dot * 4) / (rho_f * π * D_t * D_t * epsilon))*st/L)/(2*dx)

    nita_f = @. (st / (epsilon * rho_f * cp_f * (L ^ 2)))/(dx^2)
    eita_f = @. (h_v * st / (epsilon * rho_f * cp_f))
    theta_f = @. ((1.55 / D_r) * st) / (epsilon * rho_f * cp_f)
    nita_s = @. ((k_s * st) / ((1 - epsilon) * rho_s_crr * cp_s * (L ^ 2)))/(dx^2)
    eita_s = @. (h_v * st) / ((1 - epsilon) * rho_s_crr * cp_s)

    k_f_0 = -1.5e-07 * ((T_f_in*150+100)^2) - 3.3e-05 * (T_f_in*150 +100) + 0.118294

    @inbounds dTdt[1] = -Pe[1] * (T_f[2] - T_f_in) + nita_f[1] * (k_f[2]*T_f[2] - k_f[1]*2*T_f_in + k_f_0*T_f_in) +  eita_f[1] * (T_s[1] - T_f[1]) - theta_f[1] * (T_f[1] + 0.566)
    @inbounds dTdt[n] = -Pe[n] * (T_f[n] - T_f[n-1]) + nita_f[n] * (k_f[n]*T_f[n] - k_f[n]*2*T_f[n] + k_f[n-1]*T_f[n-1]) +  eita_f[n] * (T_s[n] - T_f[n]) - theta_f[n] * (T_f[n] + 0.566)
    @inbounds dTdt[n+1] = nita_s[1] * (T_s[2] - 2*T_s[1] + T_s[1]) - eita_s[1] * (T_s[1] - T_f[1])
    @inbounds dTdt[2*n] = nita_s[n] * (T_s[n] - 2*T_s[n] + T_s[n-1]) - eita_s[n] * (T_s[n] - T_f[n])

    @inbounds for i = 2:n-1
        @views dTdt[i] = -Pe[i] * (T_f[i+1] - T_f[i-1]) + nita_f[i] * (k_f[i+1]*T_f[i+1] - k_f[i]*2*T_f[i] + k_f[i-1]*T_f[i-1]) +  eita_f[i] * (T_s[i] - T_f[i]) - theta_f[i] * (T_f[i] + 0.566)
        @views dTdt[n+i] = nita_s[i] * (T_s[i+1] - 2*T_s[i] + T_s[i-1]) - eita_s[i] * (T_s[i] - T_f[i])
    end

    nothing
end

const rho_steel_nl = 7900
const cp_steel_nl = 490
const mass_cylinder_nl = 0.3713 * rho_steel_nl

f_NL_c_full = Vector{Any}()
s_NL_c_full = Vector{Any}()
f_NL_dc_full = Vector{Any}()
s_NL_dc_full = Vector{Any}()

t_c_full = Vector{Any}()
t_dc_full = Vector{Any}()
T_seq_c_full = Vector{Any}()
T_seq_dc_full = Vector{Any}()
m_seq_c_full = Vector{Any}()
m_seq_dc_full = Vector{Any}()

T0_nl = [zeros(n_nl); zeros(n_nl)]

function NONLINEAR_MODEL(T0_nl, n_nl, n_cycles, t_cs, T_c_seqs, t_dcs, T_dc_seqs, m_c_seqs, m_dc_seqs, dx_nl, L, st, D_t, D_r, epsilon, mass_cylinder_nl, cp_steel_nl, time_total_c, time_total_dc)
    for i=1:n_cycles

        T_c_func = Interpolations.LinearInterpolation(t_cs[i], T_c_seqs[i])
        T_dc_func = Interpolations.LinearInterpolation(t_dcs[i], T_dc_seqs[i])
        m_c_func = Interpolations.LinearInterpolation(t_cs[i], m_c_seqs[i])
        m_dc_func = Interpolations.LinearInterpolation(t_dcs[i], m_dc_seqs[i])
        
        #
        # Charging
        #

        tspan = (0.0, time_total_c)
        p_nl_1 = (dx_nl, n_nl, L, st, D_t, D_r, epsilon, mass_cylinder_nl, cp_steel_nl, m_c_func, T_c_func)
        prob_nl_1 = ODEProblem(NonLinear_model!, T0_nl, tspan, p_nl_1)

        sol_nl_1 = solve(prob_nl_1, RDPK3Sp35(), reltol=1e-4, saveat=t_cs[i])

        t_nl_1 = sol_nl_1.t
        T_nl_1 = Array(sol_nl_1)

        f_NL_c = T_nl_1[1:n_nl,:]
        s_NL_c = T_nl_1[n_nl+1:2*n_nl,:]

        T0_nl = [reverse(f_NL_c[:,end]); reverse(s_NL_c[:,end])]

        #
        # Discharging
        #

        tspan = (0.0, time_total_dc)
        p_nl_2 = (dx_nl, n_nl, L, st, D_t, D_r, epsilon, mass_cylinder_nl, cp_steel_nl, m_dc_func, T_dc_func)
        prob_nl_2 = ODEProblem(NonLinear_model!, T0_nl, tspan, p_nl_2)

        sol_nl_2 = solve(prob_nl_2, RDPK3Sp35(), reltol=1e-4, saveat=t_dcs[i])

        t_nl_2 = sol_nl_2.t
        T_nl_2 = Array(sol_nl_2)

        f_NL_dc = T_nl_2[1:n_nl,:]
        s_NL_dc = T_nl_2[n_nl+1:2*n_nl,:]

        T0_nl = [reverse(f_NL_dc[:,end]); reverse(s_NL_dc[:,end])]

        #
        #
        #

        push!(f_NL_c_full, collect(f_NL_c .* 150 .+ 100))
        push!(s_NL_c_full, collect(s_NL_c .* 150 .+ 100))

        push!(f_NL_dc_full, collect(f_NL_dc .* 150 .+ 100))
        push!(s_NL_dc_full, collect(s_NL_dc .* 150 .+ 100))

        push!(t_c_full, collect(t_cs[i]))
        push!(t_dc_full, collect(t_dcs[i]))
        push!(T_seq_c_full, collect(T_c_seqs[i]))
        push!(T_seq_dc_full, collect(T_dc_seqs[i]))
        push!(m_seq_c_full, collect(m_c_seqs[i]))
        push!(m_seq_dc_full, collect(m_dc_seqs[i]))

    end
end

function NONLINEAR_MODEL_b!(T0_nl, n_nl, n_cycles, t_cs, T_c_seqs, t_dcs, T_dc_seqs, m_c_seqs, m_dc_seqs, dx_nl, L, st, D_t, D_r, epsilon, mass_cylinder_nl, cp_steel_nl, time_total_c, time_total_dc)
    for i=1:n_cycles

        T_c_func = Interpolations.LinearInterpolation(t_cs[i], T_c_seqs[i])
        T_dc_func = Interpolations.LinearInterpolation(t_dcs[i], T_dc_seqs[i])
        m_c_func = Interpolations.LinearInterpolation(t_cs[i], m_c_seqs[i])
        m_dc_func = Interpolations.LinearInterpolation(t_dcs[i], m_dc_seqs[i])
        
        #
        # Charging
        #

        tspan = (0.0, time_total_c)
        p_nl_1 = (dx_nl, n_nl, L, st, D_t, D_r, epsilon, mass_cylinder_nl, cp_steel_nl, m_c_func, T_c_func)
        prob_nl_1 = ODEProblem(NonLinear_model!, T0_nl, tspan, p_nl_1)

        sol_nl_1 = solve(prob_nl_1, RDPK3Sp35(), reltol=1e-4, saveat=t_cs[i])

        t_nl_1 = sol_nl_1.t
        T_nl_1 = Array(sol_nl_1)

        f_NL_c = T_nl_1[1:n_nl,:]
        s_NL_c = T_nl_1[n_nl+1:2*n_nl,:]

        T0_nl = [reverse(f_NL_c[:,end]); reverse(s_NL_c[:,end])]

        #
        # Discharging
        #

        tspan = (0.0, time_total_dc)
        p_nl_2 = (dx_nl, n_nl, L, st, D_t, D_r, epsilon, mass_cylinder_nl, cp_steel_nl, m_dc_func, T_dc_func)
        prob_nl_2 = ODEProblem(NonLinear_model!, T0_nl, tspan, p_nl_2)

        sol_nl_2 = solve(prob_nl_2, RDPK3Sp35(), reltol=1e-4, saveat=t_dcs[i])

        t_nl_2 = sol_nl_2.t
        T_nl_2 = Array(sol_nl_2)

        f_NL_dc = T_nl_2[1:n_nl,:]
        s_NL_dc = T_nl_2[n_nl+1:2*n_nl,:]

        T0_nl = [reverse(f_NL_dc[:,end]); reverse(s_NL_dc[:,end])]
    end
end

NONLINEAR_MODEL(T0_nl, n_nl, n_cycles, t_cs_test, T_c_seqs_test, t_dcs_test, T_dc_seqs_test, m_c_seqs_test, m_dc_seqs_test, dx_nl, L, st, D_t, D_r, epsilon, mass_cylinder_nl, cp_steel_nl, time_total_c, time_total_dc)

#
#
#

data_dict = Dict(
    "f_L_c_full" => collect(f_L_c_full),
    "s_L_c_full" => collect(s_L_c_full),
    "f_H_c_full" => collect(f_H_c_full),
    "s_H_c_full" => collect(s_H_c_full),
    "f_NL_c_full" => collect(f_NL_c_full),
    "s_NL_c_full" => collect(s_NL_c_full),
    "f_L_dc_full" => collect(f_L_dc_full),
    "s_L_dc_full" => collect(s_L_dc_full),
    "f_H_dc_full" => collect(f_H_dc_full),
    "s_H_dc_full" => collect(s_H_dc_full),
    "f_NL_dc_full" => collect(f_NL_dc_full),
    "s_NL_dc_full" => collect(s_NL_dc_full),
    "T_seq_c_full" => collect(T_seq_c_full),
    "T_seq_dc_full" => collect(T_seq_dc_full),
    "m_seq_c_full" => collect(m_seq_c_full),
    "m_seq_dc_full" => collect(m_seq_dc_full),
    "t_c_full" => collect(t_c_full),
    "t_dc_full" => collect(t_dc_full))
    

matwrite("compare_L_NL.mat", data_dict)

#
# Errors
#

f_H_c_full = f_H_c_full'
f_H_dc_full = f_H_dc_full'
f_H = f_H_c_full[1]
f_H = vcat(f_H, f_H_dc_full[1])
for i=2:n_cycles
    f_H = vcat(f_H, f_H_c_full[i])
    f_H = vcat(f_H, f_H_dc_full[i])
end
f_H

s_H_c_full = s_H_c_full'
s_H_dc_full = s_H_dc_full'
s_H = s_H_c_full[1]
s_H = vcat(s_H, s_H_dc_full[1])
for i=2:n_cycles
    s_H = vcat(s_H, s_H_c_full[i])
    s_H = vcat(s_H, s_H_dc_full[i])
end
s_H

#

f_L_c_full = f_L_c_full'
f_L_dc_full = f_L_dc_full'
f_L = f_L_c_full[1]
f_L = vcat(f_L, f_L_dc_full[1])
for i=2:n_cycles
    f_L = vcat(f_L, f_L_c_full[i])
    f_L = vcat(f_L, f_L_dc_full[i])
end
f_L

s_L_c_full = s_L_c_full'
s_L_dc_full = s_L_dc_full'
s_L = s_L_c_full[1]
s_L = vcat(s_L, s_L_dc_full[1])
for i=2:n_cycles
    s_L = vcat(s_L, s_L_c_full[i])
    s_L = vcat(s_L, s_L_dc_full[i])
end
s_L

#

f_NL_c_full = f_NL_c_full'
f_NL_dc_full = f_NL_dc_full'
f_NL = f_NL_c_full[1]
f_NL = vcat(f_NL, f_NL_dc_full[1])
for i=2:n_cycles
    f_NL = vcat(f_NL, f_NL_c_full[i])
    f_NL = vcat(f_NL, f_NL_dc_full[i])
end
f_NL

s_NL_c_full = s_NL_c_full'
s_NL_dc_full = s_NL_dc_full'
s_NL = s_NL_c_full[1]
s_NL = vcat(s_NL, s_NL_dc_full[1])
for i=2:n_cycles
    s_NL = vcat(s_NL, s_NL_c_full[i])
    s_NL = vcat(s_NL, s_NL_dc_full[i])
end
s_NL

#
#

println("MSE - NL vs L: ",(calculate_error(mse, f_L, f_NL)+calculate_error(mse, s_L, s_NL))/2)
println("R2 - NL vs L: ",(calculate_error(r2, f_L, f_NL)+calculate_error(r2, s_L, s_NL))/2)
println("Error percent - NL vs L: ",(calculate_error(error_percentage, f_L, f_NL)+calculate_error(error_percentage, s_L, s_NL))/2)

println("MSE - NL vs H: ",(calculate_error(mse, f_H, f_NL)+calculate_error(mse, s_H, s_NL))/2)
println("R2 - NL vs H: ",(calculate_error(r2, f_H, f_NL)+calculate_error(r2, s_H, s_NL))/2)
println("Error percent - NL vs H: ",(calculate_error(error_percentage, f_H, f_NL)+calculate_error(error_percentage, s_H, s_NL))/2)

N = 50
plot(x_nl, f_NL[N, :], xlims=(0.0,1.0), ylims=(100.0,250.0))
#plot!(x_nl, f_L[N,:], xlims=(0.0,1.0), ylims=(100.0,250.0))
plot!(x_nl, f_H[N,:], xlims=(0.0,1.0), ylims=(100.0,250.0))






#
#

function mse_func(x, y)
    return mean((x .- y).^2)
end

function rmse_func(A, B)
    return sqrt(mse_func(A, B))
end

function calculate_mse_per_row(A, B)
    n = size(A, 1)
    mses = [mse_func(A[i, :], B[i, :]) for i in 1:n]
    return mses
end

mses = calculate_mse_per_row(f_NL_dc_full[1], f_H_dc_full[1])

plot(mses)
histogram(mses, bins=10, title="Distribution of MSE Errors", xlabel="MSE", ylabel="Frequency")

N = 50
plot(x_nl, f_NL[N, :], xlims=(0.0,1.0), ylims=(100.0,250.0))

plot!(x_nl, f_H[N,:], xlims=(0.0,1.0), ylims=(100.0,250.0))


plot(x_h, f_NL_dc_full[1][indices, N], xlims=(0.0,1.0), ylims=(0.0,1.0))
plot!(x_h, f_H_dc_full[1][indices, N], xlims=(0.0,1.0), ylims=(0.0,1.0))


calculate_rmse((s_NL_dc_full[1][indices, :]) .* 150 .+ 100, (s_H_dc_full[1]) .* 150 .+ 100)
calculate_rmse((s_NL_dc_full[1][indices, :]) .* 150 .+ 100, (s_L_dc_full[1][indices, :]) .* 150 .+ 100)

f_NL_dc_full[2]

N = 175
plot(x_h, f_L_dc_full[1][indices, N], xlims=(0.0,1.0), ylims=(0.0,1.0))
plot!(x_h, f_NL_dc_full[1][indices, N], xlims=(0.0,1.0), ylims=(0.0,1.0))
plot!(x_h, f_H_dc_full[1][:, N], xlims=(0.0,1.0), ylims=(0.0,1.0))