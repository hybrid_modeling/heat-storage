using DifferentialEquations, ModelingToolkit, Interpolations, OrdinaryDiffEq
using MAT, LinearAlgebra, Plots, PreallocationTools, Statistics, BenchmarkTools, StaticArrays, NaNMath

function T_seq_gen(t, minValue, maxValue)
    minDuration = 0.3
    maxDuration = 0.6
    maxConstantDuration = 0.15

    totalTime = t[end]
    remainingTime = totalTime
    currentMinValue = minValue
    y = zeros(length(t))

    currentIndex = 1
    lastPhaseType = -1

    while remainingTime > 0 && currentIndex <= length(t)
        phaseDuration = minDuration + (maxDuration - minDuration) * rand()
        phaseDuration = min(phaseDuration, remainingTime)

        endTime = t[currentIndex] + phaseDuration
        if endTime > totalTime
            endTime = totalTime
        end

        endIndex = findfirst(x -> x >= endTime, t)
        if isnothing(endIndex)
            endIndex = length(t)
        end

        numSamples = endIndex - currentIndex + 1

        if numSamples <= 1
            numSamples = 2
        end

        # Ensure that the constant phase does not repeat consecutively
        if lastPhaseType == 2
            phaseTypeOptions = [1, 3]
        else
            phaseTypeOptions = [1, 2, 3]
        end
        phaseType = phaseTypeOptions[rand(1:length(phaseTypeOptions))]

        x = range(-6, stop=6, length=numSamples)
        if phaseType == 1
            finalValue = currentMinValue + (maxValue - currentMinValue) * rand()
            phaseValues = currentMinValue .+ (finalValue - currentMinValue) .* (1.0 ./ (1.0 .+ exp.(-x)))
            currentMinValue = finalValue
        elseif phaseType == 2
            phaseDuration = min(phaseDuration, maxConstantDuration)
            phaseValues = fill(currentMinValue, numSamples)
        elseif phaseType == 3
            finalValue = minValue + (currentMinValue - minValue) * rand()
            phaseValues = finalValue .+ (currentMinValue - finalValue) .* (1.0 ./ (1.0 .+ exp.(x)))
            currentMinValue = finalValue
        end

        if currentIndex + length(phaseValues) - 1 <= length(y)
            y[currentIndex:currentIndex+length(phaseValues)-1] = phaseValues
        else
            y[currentIndex:end] = phaseValues[1:(length(y)-currentIndex+1)]
        end

        currentIndex += length(phaseValues)
        if currentIndex > length(t)
            break
        end
        remainingTime = totalTime - t[currentIndex - 1]
        lastPhaseType = phaseType
    end

    return y
end

function m_seq_gen(timeVec, minValue, maxValue)
    output = zeros(length(timeVec))
    
    currentTime = timeVec[1]
    currentValue = (maxValue - minValue) * rand() + minValue
    
    i = 1
    while currentTime < timeVec[end]
        stepDuration = rand() * (1.0 - 0.8) + 0.8
        
        nextTime = currentTime + stepDuration
        
        while i <= length(timeVec) && timeVec[i] <= nextTime
            output[i] = currentValue
            i += 1
        end
        
        currentTime = nextTime
        currentValue = (maxValue - minValue) * rand() + minValue
    end
    
    return output
end

function Linear_model!(dTdt, T, p, t)

    (dx, n, epsilon, L, D_t, st, rho_f, cp_f, k_f, mu_f, cp_s, rho_s_crr, nita_f, theta_f, nita_s, m_in_func, T_in_func) = p

    m_dot =  m_in_func(t)
    T_f_in = T_in_func(t)

    T_f = @view T[1:n]
    T_s = @view T[n+1:2*n]
    
    h_v = (6 * (1 - epsilon) * k_f * (2 + 1.1 * (((m_dot * D_r * 4) / (mu_f * π * D_t * D_t)) ^ 0.6) * (((mu_f * cp_f) / k_f) ^ 0.3333))) / (D_r * D_r)
    Pe = (((m_dot * 4) / (rho_f * π * D_t * D_t * epsilon))*st/L)/(2*dx)
    nita_s_ = nita_s/(dx^2)
    nita_f_ = nita_f/(dx^2)
    eita_f = ((h_v * st) / (epsilon * rho_f * cp_f))
    eita_s = ((h_v * st) / ((1 - epsilon) * rho_s_crr * cp_s))

    @inbounds dTdt[1] = -Pe * (T_f[2] - T_f_in) + nita_f_ * (T_f[2] - 2*T_f_in + T_f_in) +  eita_f * (T_s[1] - T_f[1]) - theta_f * (T_f[1] + 0.566)
    @inbounds dTdt[n] = -Pe * (T_f[n] - T_f[n-1]) + nita_f_ * (T_f[n] - 2*T_f[n] + T_f[n-1]) +  eita_f * (T_s[n] - T_f[n]) - theta_f * (T_f[n] + 0.566)
    @inbounds dTdt[n+1] = nita_s_ * (T_s[2] - 2*T_s[1] + T_s[1]) - eita_s * (T_s[1] - T_f[1])
    @inbounds dTdt[2*n] = nita_s_ * (T_s[n] - 2*T_s[n] + T_s[n-1]) - eita_s * (T_s[n] - T_f[n])

    @inbounds for i = 2:n-1
        @views dTdt[i] = -Pe * (T_f[i+1] - T_f[i-1]) + nita_f_ * (T_f[i+1] - 2*T_f[i] + T_f[i-1]) +  eita_f * (T_s[i] - T_f[i]) - theta_f * (T_f[i] + 0.566)
        @views dTdt[n+i] = nita_s_ * (T_s[i+1] - 2*T_s[i] + T_s[i-1]) - eita_s * (T_s[i] - T_f[i])
    end
    
    nothing
end

function NonLinear_model!(dTdt, T, p, t)
    (dx, n, L, st, D_t, D_r, epsilon, mass_cyclinder, cp_steel, m_in_func, T_in_func) = p

    m_dot = m_in_func(t)
    T_f_in = T_in_func(t)

    T_f = @view T[1:n]
    T_s = @view T[n+1:2*n]

    T_f_ = @. T_f * 150 + 100
    T_s_ = @. T_s * 150 + 100

    T_f_1 = @. max(T_f_, 1e-6)
    T_s_1 = @. max(T_s_, 1e-6)

    rho_f = @. -0.000321 * (T_f_1^2) - 0.614254 * T_f_1 + 1020.62
    k_f = @. -1.5e-07 * (T_f_1^2) - 3.3e-05 * T_f_1 + 0.118294
    cp_f = @. 8.970785e-04 * (T_f_1^2) + 3.313 * T_f_1 + 1496.005
    mu_f = @. exp(-2.048 * log(T_f_1) + 10.773) * 1e-03

    cp_s = @. 0.8841 * T_s_1 + 795.9
    rho_s = 2595
    k_s = 5.5
    rho_s_crr = @. rho_s + (mass_cyclinder * cp_steel / ((1 - epsilon) * cp_s * 0.3713))

    Re = @. (m_dot * D_r * 4) / (mu_f * π * D_t * D_t)
    Pr = @. (mu_f * cp_f) / k_f
    h_v = @. (6 *(1-epsilon) * k_f * (2 + 1.1 * (Re^0.6) * (Pr^0.3333))) / (D_r*D_r)

    Pe = @. (((m_dot * 4) / (rho_f * π * D_t * D_t * epsilon))*st/L)/(2*dx)

    nita_f = @. (st / (epsilon * rho_f * cp_f * (L ^ 2)))/(dx^2)
    eita_f = @. (h_v * st / (epsilon * rho_f * cp_f))
    theta_f = @. ((1.55 / D_r) * st) / (epsilon * rho_f * cp_f)
    nita_s = @. ((k_s * st) / ((1 - epsilon) * rho_s_crr * cp_s * (L ^ 2)))/(dx^2)
    eita_s = @. (h_v * st) / ((1 - epsilon) * rho_s_crr * cp_s)

    k_f_0 = -1.5e-07 * ((T_f_in*150+100)^2) - 3.3e-05 * (T_f_in*150 +100) + 0.118294

    @inbounds dTdt[1] = -Pe[1] * (T_f[2] - T_f_in) + nita_f[1] * (k_f[2]*T_f[2] - k_f[1]*2*T_f_in + k_f_0*T_f_in) +  eita_f[1] * (T_s[1] - T_f[1]) - theta_f[1] * (T_f[1] + 0.566)
    @inbounds dTdt[n] = -Pe[n] * (T_f[n] - T_f[n-1]) + nita_f[n] * (k_f[n]*T_f[n] - k_f[n]*2*T_f[n] + k_f[n-1]*T_f[n-1]) +  eita_f[n] * (T_s[n] - T_f[n]) - theta_f[n] * (T_f[n] + 0.566)
    @inbounds dTdt[n+1] = nita_s[1] * (T_s[2] - 2*T_s[1] + T_s[1]) - eita_s[1] * (T_s[1] - T_f[1])
    @inbounds dTdt[2*n] = nita_s[n] * (T_s[n] - 2*T_s[n] + T_s[n-1]) - eita_s[n] * (T_s[n] - T_f[n])

    @inbounds for i = 2:n-1
        @views dTdt[i] = -Pe[i] * (T_f[i+1] - T_f[i-1]) + nita_f[i] * (k_f[i+1]*T_f[i+1] - k_f[i]*2*T_f[i] + k_f[i-1]*T_f[i-1]) +  eita_f[i] * (T_s[i] - T_f[i]) - theta_f[i] * (T_f[i] + 0.566)
        @views dTdt[n+i] = nita_s[i] * (T_s[i+1] - 2*T_s[i] + T_s[i-1]) - eita_s[i] * (T_s[i] - T_f[i])
    end

    nothing
end

function find_nearest_indices(x_ref::AbstractArray{Float64}, x_act::AbstractArray{Float64})::Vector{Int}
    nearest_indices = Vector{Int}(undef, length(x_act))

    for (i, x) in enumerate(x_act)
        nearest_index = argmin(abs.(x_ref .- x))
        nearest_indices[i] = nearest_index
    end

    return nearest_indices
end

function collect_elements(vectors)
    return [collect(v) for v in vectors]
end

const D_t = 1.0
const D_r = 0.03
const epsilon = 0.27
const L = 3.0
const st = 1500
const rho_steel = 7900
const cp_steel = 490
const mass_cyclinder = 0.3713 * rho_steel

const rho_f_l = 845
const cp_f_l = 2380
const k_f_l = 0.1
const mu_f_l = 5.7e-4

const rho_s_l = 2595
const cp_s_l = 900
const k_s_l = 5.5
const rho_s_crr_l = rho_s_l + ((0.3713 * 7900)) * cp_steel / ((1 - epsilon) * cp_s_l * 0.3713)

const nita_f_l = (k_f_l * st) / (epsilon * rho_f_l * cp_f_l * (L ^ 2))
const theta_f_l = ((1.55 / D_r) * st) / (epsilon * rho_f_l * cp_f_l)
const nita_s_l = (k_s_l * st) / ((1 - epsilon) * rho_s_crr_l * cp_s_l * (L ^ 2))

time_total_c = 15
time_total_dc = 15

n_l = 20
x_l = range(0.0, stop=1.0, length=n_l)
dx_l = 1/n_l

n_nl = 1000
x_nl = range(0.0, stop=1.0, length=n_nl)
dx_nl = 1/n_nl

indices = find_nearest_indices(x_nl, x_l)

seq_idx = 10
n_cycles = 4

for i=1:seq_idx

    println(i)

    T0_l = [zeros(n_l); zeros(n_l)]
    T0_nl = [zeros(n_nl); zeros(n_nl)]

    f_NL_c_full = Vector{Any}()
    s_NL_c_full = Vector{Any}()
    
    f_NL_dc_full = Vector{Any}()
    s_NL_dc_full = Vector{Any}()
        
    f_L_c_full = Vector{Any}()
    s_L_c_full = Vector{Any}()

    f_L_dc_full = Vector{Any}()
    s_L_dc_full = Vector{Any}()
    
    t_c_full = Vector{Any}()
    t_dc_full = Vector{Any}()
    T_seq_c_full = Vector{Any}()
    T_seq_dc_full = Vector{Any}()
    m_seq_c_full = Vector{Any}()
    m_seq_dc_full = Vector{Any}()

    for j=1:n_cycles

        t_c = range(0.0, stop=time_total_c, length=Int(time_total_c/0.05 + 1))
        t_dc = range(0.0, stop=time_total_dc, length=Int(time_total_dc/0.05 + 1))
        
        T_seq_c = vec(T_seq_gen(t_c, 0.6, 1.0))
        T_seq_dc = vec(T_seq_gen(t_dc, 0.0, 0.2))

        m_seq_c = vec(m_seq_gen(t_c, 0.45, 0.65))
        m_seq_dc =vec(m_seq_gen(t_dc, 0.45, 0.65))

        T_seq_c = @. max(Float64(real(T_seq_c)))
        T_seq_dc = @. max(Float64(real(T_seq_dc)))
        m_seq_c = @. max(Float64(real(m_seq_c)))
        m_seq_dc = @. max(Float64(real(m_seq_dc)))

        T_c_func = Interpolations.LinearInterpolation(t_c, T_seq_c)
        T_dc_func = Interpolations.LinearInterpolation(t_dc, T_seq_dc)
        m_c_func = Interpolations.LinearInterpolation(t_c, m_seq_c)
        m_dc_func = Interpolations.LinearInterpolation(t_dc, m_seq_dc)

        #
        #
        # Non Linear
        #
        #

        # Charging

        tspan = (0.0, time_total_c)
        p_nl_1 = (dx_nl, n_nl, L, st, D_t, D_r, epsilon, mass_cyclinder, cp_steel, m_c_func, T_c_func)
        prob_nl_1 = ODEProblem(NonLinear_model!, T0_nl, tspan, p_nl_1)
        sol_nl_1 = solve(prob_nl_1, RDPK3Sp35(), reltol=1e-4, saveat=t_c)

        t_nl_1 = sol_nl_1.t
        T_nl_1 = Array(sol_nl_1)

        f_NL_c = T_nl_1[1:n_nl,:]
        s_NL_c = T_nl_1[n_nl+1:2*n_nl,:]

        T0_nl = [reverse(f_NL_c[:,end]); reverse(s_NL_c[:,end])]

        # Discharging

        tspan = (0.0, time_total_dc)
        p_nl_2 = (dx_nl, n_nl, L, st, D_t, D_r, epsilon, mass_cyclinder, cp_steel, m_dc_func, T_dc_func)
        prob_nl_2 = ODEProblem(NonLinear_model!, T0_nl, tspan, p_nl_2)
        sol_nl_2 = solve(prob_nl_2, RDPK3Sp35(), reltol=1e-4, saveat=t_dc)

        t_nl_2 = sol_nl_2.t
        T_nl_2 = Array(sol_nl_2)

        f_NL_dc = T_nl_2[1:n_nl,:]
        s_NL_dc = T_nl_2[n_nl+1:2*n_nl,:]

        T0_nl = [reverse(f_NL_dc[:,end]); reverse(s_NL_dc[:,end])]

        #
        #
        # Linear
        #
        #

        # Charging

        tspan = (0.0, time_total_c)
        p_l_1 = (dx_l, n_l, epsilon, L, D_t, st, rho_f_l, cp_f_l, k_f_l, mu_f_l, cp_s_l, rho_s_crr_l, nita_f_l, theta_f_l, nita_s_l, m_c_func, T_c_func)
        prob_l_1 = ODEProblem(Linear_model!, T0_l, tspan, p_l_1)
        sol_l_1 = solve(prob_l_1, RDPK3Sp35(), reltol=1e-4, saveat=t_c)

        t_l_1 = sol_l_1.t
        T_l_1 = Array(sol_l_1)

        f_L_c = T_l_1[1:n_l,:]
        s_L_c = T_l_1[n_l+1:2*n_l,:]

        T0_l = [reverse(f_NL_c[indices,end]); reverse(s_NL_c[indices,end])]

        # Discharging

        tspan = (0.0, time_total_dc)
        p_l_2 = (dx_l, n_l, epsilon, L, D_t, st, rho_f_l, cp_f_l, k_f_l, mu_f_l, cp_s_l, rho_s_crr_l, nita_f_l, theta_f_l, nita_s_l, m_dc_func, T_dc_func)
        prob_l_2 = ODEProblem(Linear_model!, T0_l, tspan, p_l_2)
        sol_l_2 = solve(prob_l_2, RDPK3Sp35(), reltol=1e-4, saveat=t_dc)

        t_l_2 = sol_l_2.t
        T_l_2 = Array(sol_l_2)

        f_L_dc = T_l_2[1:n_l,:]
        s_L_dc = T_l_2[n_l+1:2*n_l,:]

        T0_l = [reverse(f_NL_dc[indices,end]); reverse(s_NL_dc[indices,end])]

        #
        #
        # Storing values for each cycle
        #
        #

        push!(f_NL_c_full, collect(f_NL_c))
        push!(s_NL_c_full, collect(s_NL_c))

        push!(f_NL_dc_full, collect(f_NL_dc))
        push!(s_NL_dc_full, collect(s_NL_dc))

        push!(f_L_c_full, collect(f_L_c))
        push!(s_L_c_full, collect(s_L_c))

        push!(f_L_dc_full, collect(f_L_dc))
        push!(s_L_dc_full, collect(s_L_dc))

        push!(t_c_full, collect(t_c))
        push!(t_dc_full, collect(t_dc))
        push!(T_seq_c_full, collect(T_seq_c))
        push!(T_seq_dc_full, collect(T_seq_dc))
        push!(m_seq_c_full, collect(m_seq_c))
        push!(m_seq_dc_full, collect(m_seq_dc))

    end

    data_dict = Dict(
    "f_L_c_full" => collect(f_L_c_full),
    "s_L_c_full" => collect(s_L_c_full),
    "f_NL_c_full" => collect(f_NL_c_full),
    "s_NL_c_full" => collect(s_NL_c_full),
    "f_L_dc_full" => collect(f_L_dc_full),
    "s_L_dc_full" => collect(s_L_dc_full),
    "f_NL_dc_full" => collect(f_NL_dc_full),
    "s_NL_dc_full" => collect(s_NL_dc_full),
    "T_seq_c_full" => collect(T_seq_c_full),
    "T_seq_dc_full" => collect(T_seq_dc_full),
    "m_seq_c_full" => collect(m_seq_c_full),
    "m_seq_dc_full" => collect(m_seq_dc_full),
    "t_c_full" => collect(t_c_full),
    "t_dc_full" => collect(t_dc_full))
    
    matwrite("data_new_$(n_l)_$(i).mat", data_dict)
end
